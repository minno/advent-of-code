use common::get_lines;

use regex::Regex;

#[derive(Copy, Clone)]
enum CommandType {
    Toggle,
    TurnOn,
    TurnOff,
}

#[derive(Copy, Clone)]
struct Command {
    action: CommandType,
    coords: ((usize, usize), (usize, usize)),
}

fn get_input() -> Vec<Command> {
    let re = Regex::new(
        "(toggle|turn on|turn off) \
        (\\d+),(\\d+) \
        through \
        (\\d+),(\\d+)"
    ).unwrap();
    get_lines("inputs/day6.txt", |line| {
        let caps = re.captures(&line).unwrap();
        let lx = caps.at(2).unwrap().parse::<usize>().unwrap();
        let ly = caps.at(3).unwrap().parse::<usize>().unwrap();
        let hx = caps.at(4).unwrap().parse::<usize>().unwrap();
        let hy = caps.at(5).unwrap().parse::<usize>().unwrap();
        let action = match caps.at(1).unwrap() {
            "toggle" => CommandType::Toggle,
            "turn on" => CommandType::TurnOn,
            "turn off" => CommandType::TurnOff,
            _ => panic!("Unknown command: {}", line)
        };
        Command{ action: action, coords: ((lx, ly), (hx, hy)) }
    })
}

fn solve_first(commands: &[Command]) -> u32 {
    // Row-major: grid[i][j] => grid[i*1000 + j]
    solve_generic(commands, |command, light| {
        match command {
            CommandType::Toggle => *light = 1 - *light,
            CommandType::TurnOn => *light = 1,
            CommandType::TurnOff => *light = 0
        }
    })
}

fn solve_second(commands: &[Command]) -> u32 {
    solve_generic(commands, |command, light| {
        match command {
            CommandType::Toggle => *light += 2,
            CommandType::TurnOn => *light += 1,
            CommandType::TurnOff => *light = light.saturating_sub(1),
        }
    })
}

fn solve_generic<F>(commands: &[Command], mut switch: F) -> u32
        where F: FnMut(CommandType, &mut u32) {
    // Row-major: grid[i][j] => grid[i*1000 + j]
    let mut grid = vec![0; 1_000_000];
    for command in commands {
        let ((lx, ly), (hx, hy)) = command.coords;
        let start = lx*1000 + ly;
        let segment_len = hy - ly + 1;
        let num_segments = hx - lx + 1;
        for i in 0..num_segments {
            for j in 0..segment_len {
                let idx = start + i*1000 + j;
                switch(command.action, &mut grid[idx]);
            }
        }
    }
    grid.iter().fold(0, |a, b| a + b)
}

pub fn solve() {
    let input = get_input();
    println!("Number of lights on: {}", solve_first(&input));
    println!("Total brightness: {}", solve_second(&input));
}