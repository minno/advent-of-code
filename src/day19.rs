use common;

use std::collections::{HashMap, HashSet};
use std::iter;

use regex::Regex;

fn get_input() -> (String, HashMap<String, Vec<String>>) {
    let raw = common::get_input("inputs/day19.txt");
    let re = Regex::new(r"([:alpha:]+) => ([:alpha:]+)").unwrap();
    let mut molecule = String::new();
    let mut replacements = HashMap::new();
    for line in raw.lines() {
        if let Some(caps) = re.captures(line) {
            replacements.entry(caps.at(1).unwrap().into())
                .or_insert(Vec::new())
                .push(caps.at(2).unwrap().into());
        } else {
            molecule.push_str(line);
        }
    }
    (molecule, replacements)
}

fn all_replacements(string: &str, replacements: &HashMap<String, Vec<String>>) -> HashSet<String> {
    let mut all_replacements = HashSet::new();
    for (k, v) in replacements.iter()
            .flat_map(|(k, v)| iter::repeat(k).zip(v.iter())) {
        for (i, _) in string.match_indices(k) {
            all_replacements.insert(format!("{}{}{}", &string[..i], v, &string[i+k.len()..]));
        }
    }
    all_replacements
}

fn reverse(map: &HashMap<String, Vec<String>>) -> HashMap<String, Vec<String>> {
    let mut result = HashMap::new();
    for (k, v) in map.iter()
            .flat_map(|(k, v)| iter::repeat(k).zip(v.iter())) {
        result.entry(v.clone()).or_insert(Vec::new()).push(k.clone());
    }
    result
}

fn num_steps(goal: &str, rev_replacements: &HashMap<String, Vec<String>>) -> Option<u32> {
    if goal == "e" {
        return Some(0);
    }
    
    let possibilities = all_replacements(goal, rev_replacements);
    
    for s in &possibilities {
        if let Some(n) = num_steps(s, rev_replacements) {
            return Some(n+1);
        }
    }
    
    None
}

pub fn solve() {
    let (molecule, replacements) = get_input();
    println!("First: {}", all_replacements(&molecule, &replacements).len());
    let rev_replacements = reverse(&replacements);
    println!("Second: {}", num_steps(&molecule, &rev_replacements).unwrap());
}