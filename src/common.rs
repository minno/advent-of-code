use std::fs::File;
use std::io::{Read, BufReader, BufRead};
use std::iter::FromIterator;

pub fn get_input(filename: &str) -> String {
    let mut f = File::open(filename).unwrap();
    let mut buf = String::new();
    f.read_to_string(&mut buf).unwrap();
    buf
}

pub fn get_lines<F, C, T>(filename: &str, mut parser: F) -> C
        where F: FnMut(String) -> T, C: FromIterator<T> {
    let reader = BufReader::new(File::open(filename).unwrap());
    reader.lines().filter_map(|el| el.ok().map(|s| parser(s))).collect()
}