use std::str::CharIndices;

use common::get_lines;

fn get_input() -> Vec<String> {
    get_lines("inputs/day5.txt", |x| x)
}

struct Windows<'a> {
    full_str: &'a str,
    start_index: CharIndices<'a>,
    end_index: CharIndices<'a>,
}

impl<'a> Windows<'a> {
    fn new(string: &'a str, size: usize) -> Windows<'a> {
        let start_index = string.char_indices();
        let mut end_index = string.char_indices();
        for _ in 0..size {
            end_index.next();
        }
        Windows {
            full_str: string,
            start_index: start_index,
            end_index: end_index,
        }
    }
}

impl<'a> Iterator for Windows<'a> {
    type Item=&'a str;
    fn next(&mut self) -> Option<&'a str> {
        if let (Some((start, _)), Some((end, _))) = 
                (self.start_index.next(), self.end_index.next()) {
            Some(&self.full_str[start..end])
        } else {
            None
        }
    }
}

fn has_three_vowels(s: &str) -> bool {
    s.chars().filter(|&c| 
        "aeiou".contains(c)
    ).count() >= 3
}

fn has_repeat(s: &str) -> bool {
    let mut pairs = s.chars().zip(s.chars().skip(1));
    pairs.any(|(a, b)| a == b)
}

fn has_no_bad_pair(s: &str) -> bool {
    let bad_pairs = ["ab", "cd", "pq", "xy"];
    bad_pairs.iter().all(|pair| s.find(pair).is_none())
}

fn solve_first(input: &[String]) -> u32 {
    input.iter().filter(|s| {
        has_three_vowels(s) 
            && has_repeat(s)
            && has_no_bad_pair(s)
    }).count() as u32
}

fn has_pair_twice(input: &str) -> bool {
    let mut pairs = Windows::new(input, 2);
    pairs.any(|pair| input.matches(pair).count() >= 2)
}

fn has_spaced_repeat(input: &str) -> bool {
    let mut pairs = input.chars().zip(input.chars().skip(2));
    pairs.any(|(a, b)| a == b)
}

fn solve_second(input: &[String]) -> u32 {
    input.iter().filter(|s| {
        has_pair_twice(s) && has_spaced_repeat(s)
    }).count() as u32
}

pub fn solve() {
    let input = get_input();
    println!("First nice count: {}", solve_first(&input));
    println!("Second nice count: {}", solve_second(&input));
}