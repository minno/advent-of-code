use std::collections::HashMap;

use common::get_lines;

use regex::Regex;

fn get_input() -> HashMap<String, Op> {
    let re = Regex::new(
        "(?P<lhs>[a-z0-9]+ )?\
        (?P<op>[:alnum:]+)\
        (?P<rhs> [a-z0-9]+)? -> \
        (?P<output>[a-z]+)"
    ).unwrap();
    get_lines("inputs/day7.txt", |line| {
        let caps = re.captures(&line).unwrap();
        let output = caps.name("output").unwrap();
        let op_str = caps.name("op").unwrap();
        // Not unwrapped immediately because they may not be present
        let lhs = caps.name("lhs").map(str::trim).map(String::from);
        let rhs = caps.name("rhs").map(str::trim).map(String::from);
        let op = match op_str {
            "NOT" => Op::Not(rhs.unwrap()),
            "AND" => Op::And(lhs.unwrap(), rhs.unwrap()),
            "OR" => Op::Or(lhs.unwrap(), rhs.unwrap()),
            "LSHIFT" => Op::Lshift(lhs.unwrap(), rhs.unwrap()),
            "RSHIFT" => Op::Rshift(lhs.unwrap(), rhs.unwrap()),
            _ => Op::Literal(op_str.into())
        };
        (output.into(), op)
    })
}

#[derive(Debug)]
enum Op {
    Literal(String),
    Not(String),
    And(String, String),
    Or(String, String),
    Lshift(String, String),
    Rshift(String, String),
}

fn eval(network: &HashMap<String, Op>,
        gate: &str,
        memo: &mut HashMap<String, u16>) -> u16 {
    
    if let Ok(val) = gate.parse::<u16>() {
        return val;
    }
    
    if memo.get_mut(gate).is_none() {
        let op = &network[gate];
                
        let result = match op {
            &Op::Literal(ref input) => eval(network, input, memo),
            &Op::Not(ref input) => {
                let input = eval(network, input, memo);
                !input
            },
            &Op::And(ref lhs, ref rhs) => {
                let lhs = eval(network, lhs, memo);
                let rhs = eval(network, rhs, memo);
                lhs & rhs
            },
            &Op::Or(ref lhs, ref rhs) => {
                let lhs = eval(network, lhs, memo);
                let rhs = eval(network, rhs, memo);
                lhs | rhs
            },
            &Op::Lshift(ref lhs, ref rhs) => {
                let lhs = eval(network, lhs, memo);
                let rhs = eval(network, rhs, memo);
                lhs << rhs
            },
            &Op::Rshift(ref lhs, ref rhs) => {
                let lhs = eval(network, lhs, memo);
                let rhs = eval(network, rhs, memo);
                lhs >> rhs
            }
        };
        memo.insert(gate.into(), result);
        
        result
    } else {
        *memo.get(gate).unwrap()
    }
}

fn run(network: &HashMap<String, Op>) -> u16 {
    let mut memo = HashMap::new();
    eval(network, "a", &mut memo)
}

pub fn solve() {
    let mut network = get_input();
    let first_solution = run(&network);
    network.insert("b".into(), Op::Literal(first_solution.to_string()));
    let second_solution = run(&network);
    println!("First solution: {}", first_solution);
    println!("Second solution: {}", second_solution);
}