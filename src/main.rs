extern crate regex;
extern crate itertools;
extern crate crypto;
extern crate permutohedron;
extern crate serde_json;

mod day1;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day18;
mod day19;
mod day20;
mod day21;
/*mod day22;
mod day23;
mod day24;
mod day25;*/
mod common;

use std::env;

fn main() {
    let day = env::args().nth(1).and_then(|arg| arg.parse::<u32>().ok());
    match day {
        Some(1) => day1::solve(),
        Some(2) => day2::solve(),
        Some(3) => day3::solve(),
        Some(4) => day4::solve(),
        Some(5) => day5::solve(),
        Some(6) => day6::solve(),
        Some(7) => day7::solve(),
        Some(8) => day8::solve(),
        Some(9) => day9::solve(),
        Some(10) => day10::solve(),
        Some(11) => day11::solve(),
        Some(12) => day12::solve(),
        Some(13) => day13::solve(),
        Some(14) => day14::solve(),
        Some(15) => day15::solve(),
        Some(16) => day16::solve(),
        Some(17) => day17::solve(),
        Some(18) => day18::solve(),
        Some(19) => day19::solve(),
        Some(20) => day20::solve(),
        Some(21) => day21::solve(),
        /*Some(22) => day22::solve(),
        Some(23) => day23::solve(),
        Some(24) => day24::solve(),
        Some(25) => day25::solve(),*/
        _ => run_all(),
    };
}

fn run_all() {
    println!("\nDay 1:");
    day1::solve();
    println!("\nDay 2:");
    day2::solve();
    println!("\nDay 3:");
    day3::solve();
    println!("\nDay 4:");
    day4::solve();
    println!("\nDay 5:");
    day5::solve();
    println!("\nDay 6:");
    day6::solve();
    println!("\nDay 7:");
    day7::solve();
    println!("\nDay 8:");
    day8::solve();
    println!("\nDay 9:");
    day9::solve();
    println!("\nDay 10:");
    day10::solve();
    println!("\nDay 11:");
    day11::solve();
    println!("\nDay 12:");
    day12::solve();
    println!("\nDay 13:");
    day13::solve();
    println!("\nDay 14:");
    day14::solve();
    println!("\nDay 15:");
    day15::solve();
    println!("\nDay 16:");
    day16::solve();
    println!("\nDay 17:");
    day17::solve();
    println!("\nDay 18:");
    day18::solve();
    println!("\nDay 19:");
    day19::solve();
    println!("\nDay 20:");
    day20::solve();
    println!("\nDay 21:");
    day21::solve();
    /*println!("\nDay 22:");
    day22::solve();
    println!("\nDay 23:");
    day23::solve();
    println!("\nDay 24:");
    day24::solve();
    println!("\nDay 25:");
    day25::solve();*/
}