use std::fmt::Write;

use common::get_input;

use itertools::Itertools;

fn look_and_say(s: &str) -> String {
    let mut output = String::new();
    for (number, chunk) in &s.chars().group_by_lazy(|x| *x) {
        write!(output, "{}{}", chunk.count(), number).unwrap();
    }
    output
}

pub fn solve() {
    let mut input = get_input("inputs/day10.txt");
    for _ in 0..40 {
        input = look_and_say(&input);
    }
    println!("Length after 40: {}", input.len());
    for _ in 40..50 {
        input = look_and_say(&input);
    }
    println!("Length after 50: {}", input.len());
}