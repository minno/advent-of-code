use std::cmp::Ordering;

use common::get_lines;

fn get_input() -> Vec<u32> {
    get_lines("inputs/day17.txt", |line| line.parse().unwrap())
}

fn num_combinations(containers: &[u32], total: u32) -> u32 {
    if total == 0 { return 1; }
    
    if let Some((&head, tail)) = containers.split_first() {
        if head > total {
            num_combinations(tail, total)
        } else {
            let total_with = num_combinations(tail, total - head);
            let total_without = num_combinations(tail, total);
            total_with + total_without
        }
    } else {
        0
    }
}

fn num_combinations_at_min_count(containers: &[u32], total: u32) -> (u32, u32) {
    if total == 0 { return (1, 1); }
    
    if let Some((&head, tail)) = containers.split_first() {
        if head > total {
            num_combinations_at_min_count(tail, total)
        } else {
            let (total_with, min_with) = num_combinations_at_min_count(tail, total - head);
            let (total_without, min_without) = num_combinations_at_min_count(tail, total);
            if total_with == 0 {
                (total_without, min_without)
            } else if total_without == 0 {
                (total_with, min_with + 1)
            } else {
                match (min_with + 1).cmp(&min_without) {
                    Ordering::Less => (total_with, min_with + 1),
                    Ordering::Greater => (total_without, min_without),
                    Ordering::Equal => (total_with + total_without, min_without),
                }
            }
        }
    } else {
        (0, 0)
    }
}

pub fn solve() {
    let input = get_input();
    println!("Num combinations: {}", num_combinations(&input, 150));
    println!("Num combinations at min count: {}", num_combinations_at_min_count(&input, 150).0);
}