use std::collections::HashMap;

use common::get_lines;

use regex::Regex;

fn get_input() -> Vec<HashMap<String, u32>> {
    let re = Regex::new(r"Sue \d+: ([:alpha:]+): (\d+), ([:alpha:]+): (\d+), ([:alpha:]+): (\d+)").unwrap();
    get_lines("inputs/day16.txt", |line| {
        let caps = re.captures(&line).unwrap();
        let mut knowledge = HashMap::new();
        knowledge.insert(caps.at(1).unwrap().into(), caps.at(2).unwrap().parse().unwrap());
        knowledge.insert(caps.at(3).unwrap().into(), caps.at(4).unwrap().parse().unwrap());
        knowledge.insert(caps.at(5).unwrap().into(), caps.at(6).unwrap().parse().unwrap());
        knowledge
    })
}

fn is_consistent(full: &HashMap<&str, u32>, partial: &HashMap<String, u32>) -> bool {
    partial.iter().all(|(k, v)| { full.get(&**k) == Some(v) })
}

fn is_consistent_second(full: &HashMap<&str, u32>, partial: &HashMap<String, u32>) -> bool {
    partial.iter().all(|(k, v)| {
        let expected = *full.get(&**k).unwrap();
        if k == "cats" || k == "trees" {
            *v > expected
        } else if k == "pomeranians" || k == "goldfish" {
            *v < expected
        } else {
            *v == expected
        }
    })
}

pub fn solve() {
    let input = get_input();
    let known_info = [
        ("children", 3),
        ("cats", 7),
        ("samoyeds", 2),
        ("pomeranians", 3),
        ("akitas", 0),
        ("vizslas", 0),
        ("goldfish", 5),
        ("trees", 3),
        ("cars", 2),
        ("perfumes", 1),
    ].iter().cloned().collect::<HashMap<&str, u32>>();
    
    println!("Part 1: {}", input.iter().position(|val| is_consistent(&known_info, val)).unwrap() + 1); 
    println!("Part 2: {}", input.iter().position(|val| is_consistent_second(&known_info, val)).unwrap() + 1); 
}