use std::fmt::Write;
use std::u32;

use common::get_input;

use crypto::digest::Digest;
use crypto::md5;

// Finds the smallest integer such that the md5 hash of the seed string
// plus the string representation of the integer starts with the given
// number of zeros when represented in hexadecmial.
fn md5_leading_zeros(seed: &str, num_zeros: u32) -> u32 {
    let mut output_buf = [0u8; 16];
    // seed is 8 bytes, 32-bit number is up to 10 bytes
    let mut input_buf = String::from(seed);
    let mut hash = md5::Md5::new();
    let mut num = 0;
    loop {
        num += 1;
        write!(&mut input_buf, "{}", num).unwrap();
        hash.reset();
        hash.input(input_buf.as_bytes());
        hash.result(&mut output_buf[..]);
        // Top N/2 bytes are all zero, and the next half-byte too if N is odd.
        if output_buf.iter().take((num_zeros/2) as usize).all(|&b| b == 0)
                && (num_zeros%2 == 0 || output_buf[(num_zeros/2) as usize] & 0xF0 == 0) {
            return num;
        }
        input_buf.truncate(seed.len());
        if num == u32::MAX {
            panic!("No such hash found");
        }
    }
}

pub fn solve() {
    let input = get_input("inputs/day4.txt");
    println!("5 zeros: {}", md5_leading_zeros(&input, 5));
    println!("6 zeros: {}", md5_leading_zeros(&input, 6));
}