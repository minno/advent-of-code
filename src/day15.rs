use common::get_lines;

use regex::Regex;

use std::cmp;
use std::mem;

#[derive(Clone)]
struct Ingredient(String, i32, i32, i32, i32, i32);

fn get_input() -> Vec<Ingredient> {
    let re = Regex::new(r"([:alpha:]+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)").unwrap();
    get_lines("inputs/day15.txt", |line| {
        let caps = re.captures(&line).unwrap();
        Ingredient(
            caps.at(1).unwrap().into(),
            caps.at(2).unwrap().parse().unwrap(),
            caps.at(3).unwrap().parse().unwrap(),
            caps.at(4).unwrap().parse().unwrap(),
            caps.at(5).unwrap().parse().unwrap(),
            caps.at(6).unwrap().parse().unwrap()
        )
    })
}

fn advance(nums: &mut [u32]) -> bool {
    if nums.len() <= 1 {
        return false;
    }
    let (last, rest) = nums.split_last_mut().unwrap();
    if advance(rest) {
        true
    } else {
        if *last != 0 {
            rest.last_mut().map(|x| *x += 1);
            *last -= 1;
            true
        } else {
            mem::swap(rest.last_mut().unwrap(), last);
            false
        }
    }
}

fn score(quantities: &[u32], ingredients: &[Ingredient]) -> (u32, u32) {
    let mut totals = [0; 5];
    for (&amt, ingr) in quantities.iter().zip(ingredients.iter()) {
        let Ingredient(_, cap, dur, flav, tex, cals) = *ingr;
        totals[0] += cap * amt as i32;
        totals[1] += dur * amt as i32;
        totals[2] += flav * amt as i32;
        totals[3] += tex * amt as i32;
        totals[4] += cals * amt as i32;
    }
    let score = totals[0..4].iter().map(|&el| cmp::max(el, 0) as u32).fold(1, |a, b| a*b);
    let calories = totals[4];
    (score, calories as u32)
}

fn optimize(ingredients: &[Ingredient], is_second: bool) -> u32 {
    let mut quantities = vec![0; ingredients.len()];
    *quantities.last_mut().unwrap() = 100;
    
    let mut max = 0;
    loop {
        let (s, cals) = score(&quantities, ingredients);
        if !is_second || cals == 500 {
            max = cmp::max(s, max);
        }
        if !advance(&mut quantities) {
            break;
        }
    }
    
    max
}

pub fn solve() {
    let input = get_input();
    println!("Best cookie: {}", optimize(&input, false));
    println!("Best 500 cal cookie: {}", optimize(&input, true));
}