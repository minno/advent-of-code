use common::get_input;

pub fn solve() {
    let s = get_input("inputs/day1.txt");
    let mut ctr = 0;
    let mut basement_pos = 0;
    for (i, char) in s.chars().enumerate() {
        match char {
            '(' => ctr += 1,
            ')' => { 
                ctr -= 1;
                if ctr < 0 && basement_pos == 0 {
                    basement_pos = i + 1;
                }
            },
            _ => {}
        }
    }
    println!("Ending floor: {}", ctr);
    println!("First enters basement: {}", basement_pos);
}