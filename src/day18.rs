
use common;

fn get_input() -> Grid {
    let text = common::get_input("inputs/day18.txt");
    let xdim = text.lines().count();
    let ydim = text.lines().nth(0).unwrap().len();
    Grid::new(text.chars().filter_map(|ch| match ch {
        '#' => Some(true),
        '.' => Some(false),
        _ => None
    }), xdim, ydim)
}

#[derive(Clone, Debug)]
struct Grid {
    cells: Vec<bool>,
    xdim: usize,
    ydim: usize,
}

impl Grid {
    pub fn new<I: IntoIterator<Item=bool>>
            (cells: I, xdim: usize, ydim: usize) -> Grid {
        let cells = cells.into_iter().collect::<Vec<_>>();
        assert!(xdim * ydim == cells.len());
        Grid {
            cells: cells,
            xdim: xdim,
            ydim: ydim
        }
    }
    
    fn at(&self, x: usize, y: usize) -> bool {
        self.cells[x * self.ydim + y]
    }
    
    fn num_neighbors(&self, x: usize, y: usize) -> usize {
        let mut total = 0;
        let xmin = if x == 0 { x } else { x - 1 };
        let ymin = if y == 0 { y } else { y - 1 };
        let xmax = if x == self.xdim - 1 { x } else { x + 1 };
        let ymax = if y == self.ydim - 1 { y } else { y + 1 };
        for i in xmin..(xmax + 1) {
            for j in ymin..(ymax + 1) {
                if i == x && j == y { continue; }
                if self.at(i, j) {
                    total += 1
                }
            }
        }
        total
    }
    
    pub fn step(&mut self) {
        let mut new_grid = Vec::with_capacity(self.cells.len());
        
        for i in 0..self.xdim {
            for j in 0..self.ydim {
                let num_neighbors = self.num_neighbors(i, j);
                let is_alive = self.at(i, j);
                new_grid.push(is_alive && (num_neighbors == 2 || num_neighbors == 3)
                        || !is_alive && num_neighbors == 3);
            }
        }
        
        self.cells = new_grid;
    }
        
    pub fn set_corners_on(&mut self) {
        self.cells[0] = true;
        self.cells[self.ydim-1] = true;
        self.cells[(self.xdim-1)*self.ydim] = true;
        self.cells[self.xdim * self.ydim - 1] = true;
    }
    
    pub fn count_on(&self) -> usize {
        self.cells.iter().filter(|x| **x).count()
    }
}

pub fn solve() {
    let mut grid = get_input();
    for _ in 0..100 {
        grid.step();
    }
    println!("First: {}", grid.count_on());
    
    grid = get_input();
    grid.set_corners_on();
    for _ in 0..100 {
        grid.step();
        grid.set_corners_on();
    }
    println!("Second: {}", grid.count_on());

}