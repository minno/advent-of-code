use regex::Regex;

use common::get_lines;

struct Box {
    w: u32,
    l: u32,
    h: u32,
}

fn get_input() -> Vec<Box> {
    let re = Regex::new(r"(\d+)x(\d+)x(\d+)").unwrap();
    get_lines("inputs/day2.txt", |line| {
        let captures = re.captures(&line).unwrap();
        Box{
            w: captures.at(1).unwrap().parse().unwrap(),
            l: captures.at(2).unwrap().parse().unwrap(),
            h: captures.at(3).unwrap().parse().unwrap(),
        }
    })
}

fn small_sides(b: &Box) -> (u32, u32) {
    let mut sides = [b.w, b.l, b.h];
    sides.sort();
    (sides[0], sides[1])
}

fn paper_amt(b: &Box) -> u32 {
    let small_sides = small_sides(b);
    (2 * b.w * b.l) + 
        (2 * b.w * b.h) + 
        (2 * b.l * b.h) + 
        (small_sides.0 * small_sides.1)
}

fn ribbon_amt(b: &Box) -> u32 {
    let volume = b.w * b.l * b.h;
    let small_sides = small_sides(b);
    let small_perimeter = 2 * (small_sides.0 + small_sides.1);
    volume + small_perimeter
}

pub fn solve() {
    let boxes = get_input();
    let total_paper = boxes.iter().map(paper_amt).fold(0, |a, b| a + b);
    let total_ribbon = boxes.iter().map(ribbon_amt).fold(0, |a, b| a + b);
    println!("Paper: {}", total_paper);
    println!("Ribbon: {}", total_ribbon);
}