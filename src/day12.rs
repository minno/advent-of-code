use common::get_input;

use serde_json;
use serde_json::value::Value;

fn add_nums(data: &Value) -> u32 {
    match data {
        &Value::I64(num) => num as u32,
        &Value::U64(num) => num as u32,
        &Value::Array(ref values) => values.iter().map(add_nums).fold(0, |a,b| a+b),
        &Value::Object(ref map) => map.values().map(add_nums).fold(0, |a,b| a+b),
        _ => 0
    }
}

fn remove_red(data: &mut Value) {
    let should_null = if let Some(ref map) = data.as_object_mut() {
        map.values().any(|val| val.as_string() == Some("red"))
    } else {
        false
    };
    
    if should_null {
        *data = Value::Null;
        return;
    }
    
    match data {
        &mut Value::Array(ref mut values) => {
            values.iter_mut().map(remove_red).count();
        },
        &mut Value::Object(ref mut map) => {
            map.iter_mut().map(|(_, v)| remove_red(v)).count();
        },
        _ => {}
    }
}

pub fn solve() {
    let mut data: Value = serde_json::from_str(&get_input("inputs/day12.txt")).unwrap();
    println!("First: {}", add_nums(&data));
    remove_red(&mut data);
    println!("Second: {}", add_nums(&data));
}