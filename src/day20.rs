use common;

fn get_input() -> u32 {
    common::get_input("inputs/day20.txt").parse().unwrap()
}

fn find_with_factor_sum(limit: u32) -> u32 {
    const BLOCK_SIZE: u32 = (1 << 20);
    for block in 0.. {
        let lbound = (block*BLOCK_SIZE) as u32;
        let ubound = (lbound + BLOCK_SIZE) as u32;
        let mut factor_sums = vec![0; BLOCK_SIZE as usize];
        for step_size in 1..ubound {
            for i in (0..).take_while(|i| step_size*i < ubound) {
                if step_size*i >= lbound {
                    factor_sums[(step_size*i - lbound) as usize] += step_size*10;
                }
            }
        }
        // Zero is special
        if block == 0 {
            factor_sums[0] = 0;
        }
        println!("{:?}", &factor_sums[..10]);
        if let Some((i, _)) = factor_sums.iter().enumerate()
                .find(|&(_, &sum)| sum >= limit) {
            return i as u32 + lbound;
        }
    }
    unreachable!();
}

fn find_with_factor_sum_2(limit: u32) -> u32 {
    const BLOCK_SIZE: u32 = (1 << 20);
    for block in 0.. {
        let lbound = (block*BLOCK_SIZE) as u32;
        let ubound = (lbound + BLOCK_SIZE) as u32;
        let mut factor_sums = vec![0; BLOCK_SIZE as usize];
        for step_size in 1..ubound {
            for i in (1..).take_while(|i| step_size*i < ubound).take(50) {
                if step_size*i >= lbound {
                    factor_sums[(step_size*i - lbound) as usize] += step_size*11;
                }
            }
        }
        if let Some((i, _)) = factor_sums.iter().enumerate()
                .find(|&(_, &sum)| sum >= limit) {
            return i as u32 + lbound;
        }
    }
    unreachable!();
}

pub fn solve() {
    let n = get_input();
    println!("First: {}", find_with_factor_sum(n));
    println!("Second: {}", find_with_factor_sum_2(n));
}