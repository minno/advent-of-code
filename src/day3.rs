use std::collections::HashSet;

use itertools::Itertools;

use common::get_input;

fn run_agent<T>(input: T, visited: &mut HashSet<(i32, i32)>)
        where T: Iterator<Item=char> {
    visited.insert((0, 0));
    let mut x = 0;
    let mut y = 0;
    // Math-style, x =>, y ^
    for ch in input {
        match ch {
            'v' => y -= 1,
            '^' => y += 1,
            '<' => x -= 1,
            '>' => x += 1,
            _ => {}
        };
        visited.insert((x,y));
    }
}

fn solve_regular(input: &str) -> u32 {
    let mut visited = HashSet::new();
    run_agent(input.chars(), &mut visited);
    visited.len() as u32
}

fn solve_robo(input: &str) -> u32 {
    let mut visited = HashSet::new();
    run_agent(input.chars().step(2), &mut visited);
    run_agent(input.chars().skip(1).step(2), &mut visited);
    visited.len() as u32
}

pub fn solve() {
    let input = get_input("inputs/day3.txt");
    let regular = solve_regular(&input);
    let robo = solve_robo(&input);
    println!("Total normal: {}", regular);
    println!("Total with robo: {}", robo);
}