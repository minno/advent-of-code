use common;

use std::cmp;
use std::slice;

#[derive(Copy, Clone, Debug)]
struct Battle {
    player_health: u32,
    player_mana: u32,
    mana_spent: u32,
    boss_health: u32,
    boss_attack: u32,
    shield_timer: u8,
    poison_timer: u8,
    recharge_timer: u8,
    has_shield: bool,
}   

fn get_input() -> Battle {
    let raw = common::get_input("inputs/day22.txt");
    let words = raw.split_whitespace().collect::<Vec<_>>();
    Battle {
        player_health: 50,
        player_mana: 500,
        mana_spent: 0,
        boss_health: words[2].parse().unwrap(),
        boss_attack: words[4].parse().unwrap(),
        shield_timer: 0,
        poison_timer: 0,
        recharge_timer: 0,
        has_shield: false,
    }
}

#[derive(Copy, Clone, Debug)]
enum Spell {
    MagicMissile,
    Drain,
    Shield,
    Poison,
    Recharge,
}

impl Spell {
    fn iter() -> slice::Iter<'static, Spell> {
        static ALL_SPELLS: [Spell; 5] = [
            Spell::MagicMissile,
            Spell::Drain,
            Spell::Shield,
            Spell::Poison,
            Spell::Recharge
        ];
        
        ALL_SPELLS.iter()
    }
    
    fn cost(&self) -> u32 {
        match *self {
            Spell::MagicMissile => 53,
            Spell::Drain => 73,
            Spell::Shield => 113,
            Spell::Poison => 173,
            Spell::Recharge => 229
        }
    }
}

impl Battle {
    pub fn round(&self, spell: Spell) -> Option<Battle> {
        let mut new_status = *self;
        
        // Pre-round
        new_status.apply_effects();
        
        if new_status.boss_health == 0 {
            return Some(new_status);
        }
        
        // Player turn
        if !new_status.apply_spell(spell) {
            return None;
        }
        
        // Boss turn
        let damage = cmp::max(1, self.boss_attack - if self.has_shield {
            7
        } else {
            0
        });
        
        new_status.player_health = new_status.player_health.saturating_sub(damage);
        
        Some(new_status)
    }
    
    fn apply_effects(&mut self) {
        self.has_shield = self.shield_timer > 0;
        let has_poison = self.poison_timer > 0;
        let has_recharge = self.recharge_timer > 0;
        
        self.shield_timer = self.shield_timer.saturating_sub(1);
        self.poison_timer = self.poison_timer.saturating_sub(1);
        self.recharge_timer = self.recharge_timer.saturating_sub(1);
        
        if has_poison {
            self.boss_health = self.boss_health.saturating_sub(3);
        }
        
        if has_recharge {
            self.player_mana = self.player_mana + 101;
        }
    }
    
    fn apply_spell(&mut self, spell: Spell) -> bool {
    
        self.mana_spent += spell.cost();

        match spell {
            Spell::MagicMissile => {
                self.boss_health = self.boss_health.saturating_sub(4);
            },
            Spell::Drain => {
                self.player_health += 2;
                self.boss_health -= 2;
            },
            Spell::Shield => {
                if self.shield_timer > 0 { return false; }
                self.shield_timer = 6;
            },
            Spell::Poison => {
                if self.poison_timer > 0 { return false; }
                self.poison_timer = 6;
            },
            Spell::Recharge => {
                if self.recharge_timer > 0 { return false; }
                self.recharge_timer = 5;
            }
        }
        true
    }
    
    pub fn is_finished(&self) -> bool {
        self.player_health == 0 || self.boss_health == 0
    }
    
    pub fn winner(&self) -> Option<&'static str> {
        if self.is_finished() {
            if self.player_health == 0 {
                Some("boss")
            } else {
                Some("player")
            }
        } else {
            None
        }
    }
}

pub fn solve() {
}