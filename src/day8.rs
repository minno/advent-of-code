use common::get_lines;

use regex::Regex;

fn get_input() -> Vec<String> {
    get_lines("inputs/day8.txt", |x| x)
}

fn unescaped_difference(s: &str) -> u32 {
    let re = Regex::new(r"\\+[^\\]").unwrap();
    let mut total = 2;
    for (start, end) in re.find_iter(s) {
        let len = end - start;
        if len%2 == 1 {
            // An even number of backslashes will not escape the following character.
            total += len/2;
        } else if s.as_bytes()[end-1] == b'"' {
            // An even number of escaped backslashes followed by an escaped "
            total += len/2;
        } else {
            // An even number of escaped backslashes followed by an escaped \xFF code
            total += len/2 + 2;
        }
    }
    total as u32
}

fn escaped_difference(s: &str) -> u32 {
    (2 + s.matches('\\').count() + s.matches('"').count()) as u32 //"
}

pub fn solve() {
    let input = get_input();
    for s in input.iter().take(10) {
        println!("{}: {}", s, unescaped_difference(s));
    }
    let first = input.iter().fold(0, |sum, s| sum + unescaped_difference(s));
    let second = input.iter().fold(0, |sum, s| sum + escaped_difference(s));
    println!("Unescaped difference: {}", first);
    println!("Escaped difference: {}", second);
}