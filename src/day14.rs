use common::get_lines;

#[derive(Clone)]
struct Reindeer {
    name: String,
    speed: u32,
    time: u32,
    rest: u32,
}

fn get_input() -> Vec<Reindeer> {
    get_lines("inputs/day14.txt", |line| {
        let name = line.split_whitespace().nth(0).unwrap().into();
        let speed = line.split_whitespace().nth(3).unwrap().parse().unwrap();
        let time = line.split_whitespace().nth(6).unwrap().parse().unwrap();
        let rest = line.split_whitespace().nth(13).unwrap().parse().unwrap();
        Reindeer{ name: name, speed: speed, time: time, rest: rest }
    })
}

fn distance(time: u32, r: &Reindeer) -> u32 {
    let cycle_len = r.time + r.rest;
    let cycle_dist = r.time * r.speed;
    let part_distance = (time / cycle_len) * cycle_dist;
    let rem_time = time % cycle_len;
    if rem_time < r.time {
        part_distance + rem_time * r.speed
    } else {
        part_distance + r.time * r.speed
    }
}

fn winner(time: u32, reindeer: &[Reindeer]) -> u32 {
    let mut best_dist = 0;
    for r in reindeer {
        let dist = distance(time, r);
        if dist > best_dist {
            best_dist = dist;
        }
    }
    best_dist
}

fn simulate(time: u32, reindeer: &[Reindeer]) -> u32 {
    let mut pos = vec![0; reindeer.len()];
    let mut scores = vec![0; reindeer.len()];
    for t in 0..time {
        for (i, r) in reindeer.iter().enumerate() {
            if t % (r.time + r.rest) < r.time {
                pos[i] += r.speed;
            }
        }
        let max = *pos.iter().max().unwrap();
        for (i, s) in scores.iter_mut().enumerate() {
            if pos[i] == max {
                *s += 1;
            }
        }
    }
    *scores.iter().max().unwrap()
}

pub fn solve() {
    let end_time = 2503;
    let input = get_input();
    println!("First: {}", winner(end_time, &input));
    println!("Second: {}", simulate(end_time, &input));
}