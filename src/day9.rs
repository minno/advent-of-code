use common;

use permutohedron;

use std::cmp;
use std::u32;

fn get_input() -> Vec<Vec<u32>> {
    let input = common::get_input("inputs/day9.txt");
    let first_loc = input.split_whitespace().nth(0).unwrap();
    let num_locs = input.lines().take_while(|s| s.starts_with(first_loc)).count() + 1;
    let mut output = vec![vec![0; num_locs]; num_locs];
    let (mut i, mut j) = (0, 1);
    for line in input.lines() {
        let distance = line.split_whitespace().last().unwrap().parse().unwrap();
        output[i][j] = distance;
        output[j][i] = distance;
        j += 1;
        if j >= num_locs {
            i += 1;
            j = i + 1;
        }
    }
    output
}

fn shortest_circuit(distances: &Vec<Vec<u32>>) -> u32 {
    let mut nums = (0..distances.len()).collect::<Vec<_>>();
    let mut permutations = permutohedron::Heap::new(&mut nums);
    let mut min = u32::MAX;
    while let Some(order) = permutations.next_permutation() {
        let mut total_len = 0;
        for i in 0..order.len()-1 {
            total_len += distances[order[i]][order[i+1]];
        }
        min = cmp::min(min, total_len);
    }
    min
}

fn longest_circuit(distances: &Vec<Vec<u32>>) -> u32 {
    let mut nums = (0..distances.len()).collect::<Vec<_>>();
    let mut permutations = permutohedron::Heap::new(&mut nums);
    let mut max = u32::MIN;
    while let Some(order) = permutations.next_permutation() {
        let mut total_len = 0;
        for i in 0..order.len()-1 {
            total_len += distances[order[i]][order[i+1]];
        }
        max = cmp::max(max, total_len);
    }
    max
}

pub fn solve() {
    let input = get_input();
    println!("Shortest circuit: {}", shortest_circuit(&input));
    println!("Longest circuit: {}", longest_circuit(&input));
}