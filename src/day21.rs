

use common;

use std::cmp;
use std::mem;

use itertools::Itertools;

#[derive(Copy, Clone, Debug)]
struct Mob {
    name: &'static str,
    health: u32,
    attack: u32,
    defense: u32,
}

fn get_input() -> Mob {
    let raw = common::get_input("inputs/day21.txt");
    let words = raw.split_whitespace().collect::<Vec<_>>();
    Mob {
        name: "boss",
        health: words[2].parse().unwrap(),
        attack: words[4].parse().unwrap(),
        defense: words[6].parse().unwrap()
    }
}

//                    cost, atk, def
fn loadouts() -> Vec<(u32, (u32, u32))> {
    let weapons = [
        (8, 4),
        (10, 5),
        (25, 6),
        (40, 7),
        (74, 8)
    ];
    
    let armors = [
        (0, 0),
        (13, 1),
        (31, 2),
        (53, 3),
        (75, 4),
        (102, 5)
    ];
    
    let rings = [
        (0, (0, 0)),
        (0, (0, 0)),
        (25, (1, 0)),
        (50, (2, 0)),
        (100, (3, 0)),
        (20, (0, 1)),
        (40, (0, 2)),
        (80, (0, 3))
    ];
    
    let mut result = Vec::new();
    for &(wep_cost, wep_atk) in &weapons {
        for &(arm_cost, arm_def) in &armors {
            for (&(r1_cost, (r1_atk, r1_def)), &(r2_cost, (r2_atk, r2_def)))
                    in rings.iter().combinations() {
                let cost = wep_cost + arm_cost + r1_cost + r2_cost;
                let atk = wep_atk + r1_atk + r2_atk;
                let def = arm_def + r1_def + r2_def;
                result.push((cost, (atk, def)));
            }
        }
    }
    result
}

fn damage(attacker: Mob, defender: Mob) -> u32 {
    if defender.defense >= attacker.attack {
        1
    } else {
        attacker.attack - defender.defense
    }
}

fn winner(mut attacker: Mob, mut defender: Mob) -> &'static str {
    loop {
        let damage = damage(attacker, defender);
        if damage >= defender.health {
            return attacker.name;
        } else {
            defender.health -= damage;
        }
        mem::swap(&mut attacker, &mut defender);
    }
}

pub fn solve() {
    let boss = get_input();
    let cheapest = loadouts().iter().filter(|&&(_, (atk, def))| {
        let player = Mob { 
            name: "player",
            health: 100,
            attack: atk,
            defense: def,
        };
        winner(player, boss) == "player"
    }).fold(!0, |min, &loadout| cmp::min(min, loadout.0));
    
    let worst = loadouts().iter().filter(|&&(_, (atk, def))| {
        let player = Mob { 
            name: "player",
            health: 100,
            attack: atk,
            defense: def,
        };
        winner(player, boss) == "boss"
    }).fold(0, |max, &loadout| cmp::max(max, loadout.0));
    println!("Cheapest: {}", cheapest);
    println!("Worst: {}", worst);
    
}