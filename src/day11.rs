use common::get_input;

fn incr(s: &mut [u8]) {
    for i in (0..s.len()).rev() {
        s[i] += 1;
        if b"iol".contains(&s[i]) {
            s[i] += 1;
        }
        if s[i] > b'z' {
            s[i] = b'a';
        } else {
            break;
        }
    }
}

fn has_straight(s: &[u8]) -> bool {
    s.windows(3).any(|seq| {
        seq[1] - seq[0] == 1 && seq[2] - seq[1] == 1
    })
}

fn has_pairs(s: &[u8]) -> bool {
    let mut pair_seen = [false; 26];
    for pair in s.windows(2) {
        if pair[0] == pair[1] {
            pair_seen[(pair[0] - b'a') as usize] = true;
        }
    }
    pair_seen.iter().filter(|&&b| b).count() >= 2
}

fn has_no_bad_letter(s: &[u8]) -> bool {
    s.iter().all(|c| !b"iol".contains(c))
}

fn is_valid(s: &[u8]) -> bool {
    has_straight(s) && has_pairs(s) && has_no_bad_letter(s)
}

pub fn solve() {
    let mut input = get_input("inputs/day11.txt").into_bytes();
    loop {
        incr(&mut input);
        if is_valid(&input) {
            break;
        }
    }
    println!("New password: {}", String::from_utf8_lossy(&input));
    loop {
        incr(&mut input);
        if is_valid(&input) {
            break;
        }
    }
    println!("Newer password: {}", String::from_utf8_lossy(&input));
}